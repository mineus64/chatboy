#Chatboy Main
#Currently deprecated and not being worked on, I'm experimenting elsewhere

#Import Discord.py for Discord things
import discord
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
#Import os for handling misc stuff
import os

#Create ChatBoy
chatbot = ChatBot('ChatBoy')

#Create a new trainer for the chatbot
trainer = ChatterBotCorpusTrainer(chatbot)
#Train the chatbot based on the english corpus
try:
    trainer.train("chatterbot.corpus.english")
except:
    print("Error training with English corpus, skipping")
#Train the chatbot based on past experience
try:
    trainer.train(os.path.dirname(os.path.abspath(__file__))+"/ChatBoy_training_export.json")
except FileNotFoundError:
    print("ChatBoy Training Export not found, skipping")

#Initialise Discord client
print("ChatBoy loading...")
client = discord.Client()

#Discord bot stuff
#Stuff to do when the bot initially comes online
@client.event
async def on_ready():
    await client.change_presence(game=discord.Game(name="In Development"))
    print("ChatBoy ready!")
    return
#Stuff to do when the bot receives a message
@client.event
async def on_message(message):
    if message.author == client.user:
        return
    else:
        response = chatbot.get_response(message.content)
        await client.send_message(message.channel, response)
        #Export past experience for continuation between closes and reopens
        #trainer.export_for_training(os.path.dirname(os.path.abspath(__file__))+"/ChatBoy_training_export.json")
        #return

#Run the Discord client
client.run('NTg3NDc0NDY1ODYxMDA5NDA4.XP3GmA.C9HVIt_zMfIYz52HZ5Ba47K9ZJU')
#Don't put anything after this, it won't run until you exit the program
