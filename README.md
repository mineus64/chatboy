# ChatBoy
ChatBoy Virtual Personal Assistant

# Prerequisites
Running Chatboy requires the following prerequisites:

1: discord.py

Discord.py is used for handling bot interactions with Discord. It can be installed by running python3 -m pip install -U discord.py --user

The --user flag is fairly helpful

2: Chatterbot and Chatterbot.corpus

Chatterbot is the basic framework used to program the bot while I work my head around NLTK. Chatterbot.corpus is the supplemental data used for training the Chatterbot.

Chatterbot can be installed using python3 -m pip install -U chatterbot --user

Chatterbot.corpus can be installed using python3 -m pip install -U chatterbot.corpus --user

If you are running on Windows and experience problems with these commands, try upgrading to a proper OS.
